// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PRACTICE_MODULE_20_Practice_Module_20GameModeBase_generated_h
#error "Practice_Module_20GameModeBase.generated.h already included, missing '#pragma once' in Practice_Module_20GameModeBase.h"
#endif
#define PRACTICE_MODULE_20_Practice_Module_20GameModeBase_generated_h

#define Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_SPARSE_DATA
#define Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_RPC_WRAPPERS
#define Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPractice_Module_20GameModeBase(); \
	friend struct Z_Construct_UClass_APractice_Module_20GameModeBase_Statics; \
public: \
	DECLARE_CLASS(APractice_Module_20GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Practice_Module_20"), NO_API) \
	DECLARE_SERIALIZER(APractice_Module_20GameModeBase)


#define Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPractice_Module_20GameModeBase(); \
	friend struct Z_Construct_UClass_APractice_Module_20GameModeBase_Statics; \
public: \
	DECLARE_CLASS(APractice_Module_20GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Practice_Module_20"), NO_API) \
	DECLARE_SERIALIZER(APractice_Module_20GameModeBase)


#define Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APractice_Module_20GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APractice_Module_20GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APractice_Module_20GameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APractice_Module_20GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APractice_Module_20GameModeBase(APractice_Module_20GameModeBase&&); \
	NO_API APractice_Module_20GameModeBase(const APractice_Module_20GameModeBase&); \
public:


#define Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APractice_Module_20GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APractice_Module_20GameModeBase(APractice_Module_20GameModeBase&&); \
	NO_API APractice_Module_20GameModeBase(const APractice_Module_20GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APractice_Module_20GameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APractice_Module_20GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APractice_Module_20GameModeBase)


#define Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_12_PROLOG
#define Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_SPARSE_DATA \
	Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_RPC_WRAPPERS \
	Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_INCLASS \
	Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_SPARSE_DATA \
	Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PRACTICE_MODULE_20_API UClass* StaticClass<class APractice_Module_20GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Practice_Module_20_Source_Practice_Module_20_Practice_Module_20GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
