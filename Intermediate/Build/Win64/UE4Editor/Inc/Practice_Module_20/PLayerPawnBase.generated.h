// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PRACTICE_MODULE_20_PLayerPawnBase_generated_h
#error "PLayerPawnBase.generated.h already included, missing '#pragma once' in PLayerPawnBase.h"
#endif
#define PRACTICE_MODULE_20_PLayerPawnBase_generated_h

#define Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_SPARSE_DATA
#define Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput);


#define Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput);


#define Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPLayerPawnBase(); \
	friend struct Z_Construct_UClass_APLayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APLayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Practice_Module_20"), NO_API) \
	DECLARE_SERIALIZER(APLayerPawnBase)


#define Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPLayerPawnBase(); \
	friend struct Z_Construct_UClass_APLayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APLayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Practice_Module_20"), NO_API) \
	DECLARE_SERIALIZER(APLayerPawnBase)


#define Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APLayerPawnBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APLayerPawnBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APLayerPawnBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APLayerPawnBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APLayerPawnBase(APLayerPawnBase&&); \
	NO_API APLayerPawnBase(const APLayerPawnBase&); \
public:


#define Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APLayerPawnBase(APLayerPawnBase&&); \
	NO_API APLayerPawnBase(const APLayerPawnBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APLayerPawnBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APLayerPawnBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APLayerPawnBase)


#define Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_12_PROLOG
#define Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_SPARSE_DATA \
	Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_RPC_WRAPPERS \
	Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_INCLASS \
	Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_SPARSE_DATA \
	Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
	Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PRACTICE_MODULE_20_API UClass* StaticClass<class APLayerPawnBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Practice_Module_20_Source_Practice_Module_20_PLayerPawnBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
