// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Practice_Module_20/Practice_Module_20GameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePractice_Module_20GameModeBase() {}
// Cross Module References
	PRACTICE_MODULE_20_API UClass* Z_Construct_UClass_APractice_Module_20GameModeBase_NoRegister();
	PRACTICE_MODULE_20_API UClass* Z_Construct_UClass_APractice_Module_20GameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Practice_Module_20();
// End Cross Module References
	void APractice_Module_20GameModeBase::StaticRegisterNativesAPractice_Module_20GameModeBase()
	{
	}
	UClass* Z_Construct_UClass_APractice_Module_20GameModeBase_NoRegister()
	{
		return APractice_Module_20GameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_APractice_Module_20GameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APractice_Module_20GameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Practice_Module_20,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APractice_Module_20GameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Practice_Module_20GameModeBase.h" },
		{ "ModuleRelativePath", "Practice_Module_20GameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APractice_Module_20GameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APractice_Module_20GameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APractice_Module_20GameModeBase_Statics::ClassParams = {
		&APractice_Module_20GameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_APractice_Module_20GameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APractice_Module_20GameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APractice_Module_20GameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APractice_Module_20GameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APractice_Module_20GameModeBase, 3444056230);
	template<> PRACTICE_MODULE_20_API UClass* StaticClass<APractice_Module_20GameModeBase>()
	{
		return APractice_Module_20GameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APractice_Module_20GameModeBase(Z_Construct_UClass_APractice_Module_20GameModeBase, &APractice_Module_20GameModeBase::StaticClass, TEXT("/Script/Practice_Module_20"), TEXT("APractice_Module_20GameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APractice_Module_20GameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
