// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Practice_Module_20GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PRACTICE_MODULE_20_API APractice_Module_20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
